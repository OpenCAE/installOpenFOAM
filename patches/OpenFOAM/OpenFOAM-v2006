diff --git a/etc/config.sh/compiler b/etc/config.sh/compiler
index 41e73ee..ae51af5 100644
--- a/etc/config.sh/compiler
+++ b/etc/config.sh/compiler
@@ -25,47 +25,21 @@
 case "$WM_COMPILER_TYPE" in
 ThirdParty)
     # Default versions of GMP, MPFR, MPC - override as necessary
-    gmp_version=gmp-system
-    mpfr_version=mpfr-system
-    mpc_version=mpc-system
+    gmp_version=gmp-6.1.2
+    mpfr_version=mpfr-4.0.1
+    mpc_version=mpc-1.1.0
 
     case "$WM_COMPILER" in
-    Gcc |\
-    Gcc48*) gcc_version=gcc-4.8.5 ;;
-    Gcc49*) gcc_version=gcc-4.9.4 ;;
-    Gcc51*) gcc_version=gcc-5.1.0 ;;
-    Gcc52*) gcc_version=gcc-5.2.0 ;;
-    Gcc53*) gcc_version=gcc-5.3.0 ;;
-    Gcc54*) gcc_version=gcc-5.4.0 ;;
-    Gcc55*) gcc_version=gcc-5.5.0 ;;
-    Gcc61*) gcc_version=gcc-6.1.0 ;;
-    Gcc62*) gcc_version=gcc-6.2.0 ;;
-    Gcc63*) gcc_version=gcc-6.3.0 ;;
-    Gcc64*) gcc_version=gcc-6.4.0 ;;
-    Gcc65*) gcc_version=gcc-6.5.0 ;;
-    Gcc71*) gcc_version=gcc-7.1.0 ;;
-    Gcc72*) gcc_version=gcc-7.2.0 ;;
-    Gcc73*) gcc_version=gcc-7.3.0 ;;
-    Gcc74*) gcc_version=gcc-7.4.0 ;;
-    Gcc75*) gcc_version=gcc-7.5.0 ;;
-    Gcc81*) gcc_version=gcc-8.1.0 ;;
-    Gcc82*) gcc_version=gcc-8.2.0 ;;
-    Gcc83*) gcc_version=gcc-8.3.0 ;;
-    Gcc91*) gcc_version=gcc-9.1.0 ;;
-    Gcc92*) gcc_version=gcc-9.2.0 ;;
-
-    Clang |\
-    Clang37*) clang_version=llvm-3.7.1 ;;
-    Clang38*) clang_version=llvm-3.8.1 ;;
-    Clang39*) clang_version=llvm-3.9.1 ;;
-    Clang40*) clang_version=llvm-4.0.1 ;;
-    Clang50*) clang_version=llvm-5.0.2 ;;
-    Clang60*) clang_version=llvm-6.0.1 ;;
-    Clang70*) clang_version=llvm-7.0.1 ;;
-    Clang71*) clang_version=llvm-7.1.0 ;;
-    Clang80*) clang_version=llvm-8.0.1 ;;
-    Clang90*) clang_version=llvm-9.0.0 ;;
-
+    Gcc*)
+        version=${WM_COMPILER#Gcc}
+        version=${version//_/.}
+        gcc_version=gcc-$version
+        ;;
+    Clang*)
+        version=${WM_COMPILER#Clang}
+        version=${version//_/.}
+        clang_version=llvm-$version
+         ;;
     *)
         /bin/cat << UNKNOWN_COMPILER 1>&2
 ===============================================================================
diff --git a/etc/config.sh/mpi b/etc/config.sh/mpi
index 200b30d..a8628b3 100644
--- a/etc/config.sh/mpi
+++ b/etc/config.sh/mpi
@@ -38,7 +38,7 @@ export FOAM_MPI=dummy  # Fallback value
 case "$WM_MPLIB" in
 SYSTEMOPENMPI*)
     # The system installed openmpi, locations discovery via mpicc.
-    export FOAM_MPI=openmpi-system
+    export FOAM_MPI="$WM_MPLIB"
 
     # Undefine OPAL_PREFIX if set to one of the paths on foamOldDirs
     if [ -z "$($foamClean -env=OPAL_PREFIX "$foamOldDirs")" ]
@@ -283,7 +283,7 @@ INTELMPI*)
         # I_MPI_ROOT: The Intel MPI Library installation directory
 
         MPI_ARCH_PATH="${I_MPI_ROOT%/}" # Remove trailing slash
-        FOAM_MPI="${MPI_ARCH_PATH##*/}"
+	FOAM_MPI="$WM_MPLIB"
 
         # If subdirectory is version number only, prefix with 'impi-'
         case "$FOAM_MPI" in ([0-9]*) FOAM_MPI="impi-$FOAM_MPI";; esac
@@ -297,7 +297,7 @@ INTELMPI*)
     else
         # MPI_ROOT: General specification
         MPI_ARCH_PATH="${MPI_ROOT%/}" # Remove trailing slash
-        FOAM_MPI="${MPI_ARCH_PATH##*/}"
+	FOAM_MPI="$WM_MPLIB"
 
         # If subdirectory is version number only, prefix with 'impi-'
         case "$FOAM_MPI" in ([0-9]*) FOAM_MPI="impi-$FOAM_MPI";; esac
diff --git a/wmake/rules/linux64Gcc/mplibINTELMPI b/wmake/rules/linux64Gcc/mplibINTELMPI
index b3667bc..53a6a23 100644
--- a/wmake/rules/linux64Gcc/mplibINTELMPI
+++ b/wmake/rules/linux64Gcc/mplibINTELMPI
@@ -1,3 +1,3 @@
 PFLAGS     = -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX
 PINC       = -isystem $(MPI_ARCH_PATH)/intel64/include
-PLIBS      = -L$(MPI_ARCH_PATH)/intel64/lib -lmpi
+PLIBS      = -L$(MPI_ARCH_PATH)/intel64/lib/release -L$(MPI_ARCH_PATH)/intel64/lib -lmpi
diff --git a/wmake/rules/linux64Icc/mplibINTELMPI b/wmake/rules/linux64Icc/mplibINTELMPI
index b3667bc..53a6a23 100644
--- a/wmake/rules/linux64Icc/mplibINTELMPI
+++ b/wmake/rules/linux64Icc/mplibINTELMPI
@@ -1,3 +1,3 @@
 PFLAGS     = -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX
 PINC       = -isystem $(MPI_ARCH_PATH)/intel64/include
-PLIBS      = -L$(MPI_ARCH_PATH)/intel64/lib -lmpi
+PLIBS      = -L$(MPI_ARCH_PATH)/intel64/lib/release -L$(MPI_ARCH_PATH)/intel64/lib -lmpi
