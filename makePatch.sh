#/bin/sh

LANG=C

for dir in $*
do
    echo $dir

    OF=${dir##*/}
    version=${dir##*/OpenFOAM-}

    patch=patches/OpenFOAM/${OF}
    (cd ${dir}
	git diff 
    ) > ${patch}
    [ -s ${patch} ] || rm -f ${patch}

    patch=patches/OpenFOAM/ThirdParty-${version}
    (cd ${dir}/../ThirdParty-${version}
	git diff
    ) > ${patch}
[ -s ${patch} ] || rm -f ${patch}
done
