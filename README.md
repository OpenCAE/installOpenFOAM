## これは何か
スーパーコンピュータのように管理者権限が無いシステム上で，指定したバージョンのOpenFOAMと，依存するソフトウェアを自動的にソースからビルドしてインストールするbashスクリプト

---

### OpenFOAMのソースからのビルドは面倒
- OpenFOAMをビルドするにはcmake, flex, boost, Gccコンパイラ, GMP, MPFR, MPIライブラリなどの依存ソフトウェアが必要
- 依存ソフトウェアがシステムに無い時，ソースからビルドする必要が生じる
- 種々の依存ソフトウェアのバージョン制限を考え，適切なバージョンのソースをダウンロードし，適切な順序でビルドしていくのは面倒

---

### 自動ビルドスクリプトの特徴
- installOpenFOAMは単なるbashスクリプト
  - 管理者権限が不要なパッケージ管理システムとしては，Linux版であるLinuxbrewがあるが，rubyで記述されており，rubyのビルドも必要となる・・・
- ソースを自動でダウンロード
  - OpenFOAM，ThirdParty，依存ソフトのソースを自動ダウンロード
  - downloadディレクトリに予めインストールファイルを置いておけば，ダウンロード不可の計算機でもOK
- 依存ソフトウェアのバージョンの指定に応じて， foamConfigurePathsを用いて各種設定ファイルを変更
- コンパイラやMPIライブラリのバージョン毎にバイナリをビルド
- ビルドした依存ソフトウェアの再利用
  - 異なるOpenFOAMのバージョンにおいて既にビルドした依存ソフトウェアが見付かった場合には，シンボリックリンクを貼る(-lオプション)，もしくはコピーする(-cオプション)．ただし，デフォルトでは再ビルドを行う

---

### コンパイラやMPIライブラリのバージョン毎にバイナリをビルド

- 元々環境変数WM_COMPILERにコンパイラを指定できる(Gcc, Gcc48, Gcc49, ...)
  - wmakeのルールを修正し，バージョン番号の"."を"\_"に変換して付加して与えられるようにした
  - 自動的に指定バージョンのコンパイラを用いるようにした
  - コンパイラのバージョン毎にビルドバイナリの保存先を分けた
- MPIライブラリ(WM_MPLIB)についても同様
  - 通常はINTELMPIのように，バージョン番号までは与えない
  - INTEL2018_1_163のように指定することにより，ビルド・実行時に使用するバージョンを切り替えらえるようにした

---

## 設定ファイル
- [installjob.sh](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/installjob.sh) : インストールスクリプト
- [etc/system](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/system) : ホスト名からシステムの判別設定
- [etc/url](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/url) : ダウンロードURLの設定
- [etc/version](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/version) : ソフトウェアバージョンの設定
- system/システム名/settings : 各システム別のコンパイラやMPIライブラリの設定
- system/システム名/bashrc : OpenFOAMバージョン，COMPILER，MPLIB等の組み合わせ設定
- system/システム名/url : 各システム独自のurl(オプション)
- system/システム名/version : 各システム独自のversion(オプション)

---

### [installjob.sh](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/installjob.sh)

インストールスクリプト．
内部でビルドするバージョンを指定する．

---

### [etc/system](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/system) 

ポスト名からシステム名の自動判別を行うbashスクリプト．

---

### [etc/url](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/url)

OpenFOAMのビルドに必要となるソフトウェアのダウンロードURLを設定するbashスクリプト

---

### [etc/version](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/etc/version)
ThirdPartyのソフトウェアバージョンを設定しているbashスクリプト

---

### system/システム名/settings

各システム用にコンパイラやMPIライブラリの設定をしているbashスクリプト

---

### system/システム名/bashrc

システム毎のインストール設定を行うbashスクリプト

```
# OpenFOAM install directory                                                                                            
FOAM_INST_DIR=${HOME}/OpenFOAM

# Download Only                                                                                                         
unset DOWNLOAD_ONLY
#DOWNLOAD_ONLY=yes                                                                                                      

# Number of processors                                                                                                  
export WM_NCOMPPROCS=1

# OpenFOAM version list to install                                                                                      
OpenFOAM_BUILD_OPTION_LIST=(
    OpenFOAM_VERSION=OpenFOAM-v1712,BUILD_PARAVIEW=0\
,COMPILER_TYPE=ThirdParty,COMPILER=Gcc4_8_5,MPLIB=OPENMPI\
,COMPILE_OPTION=Opt,ARCH_OPTION=64,PRECISION_OPTION=DP,LABEL_SIZE=32
)
```

- ```FOAM_INST_DIR``` : OpenFOAMをインストールするディレクトリ
- ```DOWNLOAD_ONLY``` : ダウンロードのみの時は何からの値を設定する
- ```WM_NCOMPPROCS``` : ビルド時の並列数
- ```OpenFOAM_BUILD_OPTION_LIST``` : ビルド設定組み合わせ
    - OpenFOAM_VERSION: OpenFOAMバージョン
    - BUILD_PARAVIEW: ParaViewのビルド有無(1 or 0)
    - ビルド設定(OpenFOAMのetc/bashrc内の環境変数からWM_を除いたもの)
        - COMPILER_TYPE: コンパイラのインストール種別(system or ThirdParty)
        - COMPILER: コンパイラ(Gcc*, Icc*など)
        - MPLIB: MPIライブラリ(OPENMPI, INTELMPI*など)
        - その他: ビルド時の詳細設定
    - 実際にビルドする組み合わせはオプションで指定可能

---

## 一般的なビルド方法

### OpenFOAM用ディレクトリの作成

- 以下では```$HOME/OpenFOAM``` にOpenFOAMをインストールすると仮定

```bash
mkdir $HOME/OpenFOAM
```

### レポジトリの取得

```bash
cd $HOME/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
```
### ビルドするバージョンの指定

スクリプトinstalljob.shにおけるinstall.shの引数にビルドするバージョンを指定する．

```
#!/bin/bash
[ -n "$PBS_O_WORKDIR" ] && cd $PBS_O_WORKDIR
./install.sh 'OpenFOAM_VERSION=OpenFOAM-v2006,.*'
```

- 引数には```system/システム名/bashrc```におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンが指定可
- 引数がOpenFOAM_BUILD_OPTION_LIST配列の要素とのマッチにはexprコマンドを用いているため，上記の例のように正規表現で記述することが可能
- 引数を指定しないと，OpenFOAM_BUILD_OPTION_LIST配列で定義した全てのバージョンをビルド
- OpenFOAM_BUILD_OPTION_LIST配列は網羅的に定義しているわけではないので，各自必要ビルドバージョンの定義を追加すること
---

### ログインノードでのビルド

```bash
./installjob.sh >& log.installjob.sh &
tail -f log.installjob.sh
```

- 通常ログインノードでは，並列ビルドが実行できないので，後述するシステム毎のインストール方法を参考に並列ビルドを行なったほうが，ビルドが高速に実行できる．

---

### インストールしたビルドバージョンの環境設定

'.'(ドット)またはsourceコマンドを用いて，install.shを-sオプションを付きで実行すると，引数で指定したバージョンのOpenFOAMの環境設定のみを行う．

```
. $HOME/OpenFOAM/installOpenFOAM/install.sh -s 'OpenFOAM_VERSION=OpenFOAM-v2006,.*'
```

---

## システム毎のインストール方法
- 計算科学振興財団 [FOCUS](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/FOCUS/README.md)
- 東京大学 [Reedbush-U](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Reedbush-U/README.md)
- 東京大学 [Oakbridge-CX](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Oakbridge-CX/README.md)
- JCAHPC [Oakforest-PACS](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Oakforest-PACS/README.md)
- 九州大学 [ITO](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/ITO/README.md)
- 大阪大学 [OCTOPUS](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/OCTOPUS/README.md)
