#!/bin/bash

[ $BASH_SOURCE ] && INSTALLOPENFOAM_DIR=$(\cd ${BASH_SOURCE%/*} && \pwd -P) || \
INSTALLOPENFOAM_DIR=$HOME/OpenFOAM/installOpenFOAM

#
# Usage
#
usage()
{
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    echo
    cat<<USAGE
Usage: [.] ${0##*/} [OPTION] [VERSION]..[VERSION]
options:
  -a application(,application) wmake application using auto_wmake
  -e                 stop on error
  --copy|-c          copy same third party package if it exists in another ThirdParty directory
  --link|-l          make symblock link to same third party package if it exists in another ThirdParty directory
  --remove-xHost|-r  remove -xHost pptimization option for C/C++ compiler.
  --source|-s        source OpenFOAM setting of VERSION. '.' is required. eg. '. install.sh -s VERSION'
  --help|-h          print the usage
note: link option and copy option are exclusive.

VERSION: Build version among OpenFOAM_BUILD_OPTION_LIST in system/SYSTEM/bashrc. Regular expression is available.

USAGE
    exit 1
}

#
# Write error message and exit
#
error()
{
    echo "Error: $1"
    exit 1
}

#
# Download file $1 from url $2 into download/ directory
#
downloadFile()
{
    [ "$#" -eq 2 ] || {
        echo "downloadFile called with incorrect number of arguments $@"
        return 1
    }

    local file="$1"
    local url="$2"

    [ -d download ] || mkdir download

    if [ ! -e download/$file ];then
        echo "downloading $file from $url"
	(
	    trap 'rm -f $file;error "Unable to download $file from $url"' 1 2 3 15
	    cd download && wget --no-check-certificate "$url" -O "$file"
	)
    fi
}

download_and_extract_source()
{
    for DIRECTORY in OpenFOAM ThirdParty
    do
	local PACKAGE=$DIRECTORY-$FOAM_VERSION
	if [ ! -d $FOAM_INSTALL_DIRECTORY/$PACKAGE ];then
	    local source_file=$PACKAGE.tgz
	    if [ $DIRECTORY = "OpenFOAM" ];then
		local URL="$(openfoam_url $PACKAGE)"
	    else
		local URL="$(thirdparty_url $PACKAGE)"
	    fi
	    downloadFile "$source_file" "$URL"

	    tar xpf download/$source_file -C $FOAM_INSTALL_DIRECTORY
	    for REPOSITORY_VERSION in ${FOAM_VERSION%.*}.x ${FOAM_VERSION%.*}
	    do
		if [ -d $FOAM_INSTALL_DIRECTORY/$DIRECTORY-$REPOSITORY_VERSION-version-$FOAM_VERSION ];then
		    mv $FOAM_INSTALL_DIRECTORY/$DIRECTORY-$REPOSITORY_VERSION-version-$FOAM_VERSION \
			$FOAM_INSTALL_DIRECTORY/$PACKAGE
		fi
	    done
	    (
		cd $FOAM_INSTALL_DIRECTORY/$PACKAGE
		git init
		git config user.name "The Open CAE Society of Japan"
		git config user.email repository@opencae.or.jp
		if [ $DIRECTORY = "ThirdParty" ]
		then
		    git add Allclean Allwmake etc make* 
		else
		    git add Allwmake etc wmake
		fi
		git commit -m "Original version."
	    )
	else
	    echo "$PACKAGE is already extracted in $FOAM_INSTALL_DIRECTORY"
	fi
    done
}

patch_source()
{
    [ -n "$DOWNLOAD_ONLY" ] && return 0

    for DIRECTORY in OpenFOAM ThirdParty
    do
	local PACKAGE=$DIRECTORY-$FOAM_VERSION
	local patchFile="$PWD/patches/OpenFOAM/$DIRECTORY-$FOAM_VERSION"
	if [ -f $patchFile ];then
	    (
		cd $FOAM_INSTALL_DIRECTORY/$PACKAGE
		local sentinel="PATCHED_DURING_OPENFOAM_BUILD"
		if [ -f "$sentinel" ];then
		    echo "patch for $PACKAGE was already applied"
		else
		    echo "apply patch $patchFile for $PACKAGE"
		    touch "$sentinel"
		    patch -b -l -p1 < $patchFile 2>&1 | tee $sentinel
		fi
	    )
	fi
    done

    for patchFile in $PWD/patches/ThirdParty/*
    do
	local PACKAGE=${patchFile##*/}
	[ ${PACKAGE%-*} = "paraview" ] && PACKAGE="${PACKAGE/paraview/ParaView}"
	local dir=$FOAM_INSTALL_DIRECTORY/ThirdParty-$FOAM_VERSION/$PACKAGE
	echo $dir
	if [ -d $dir ];then
	    (
		cd $dir
		local sentinel="PATCHED_DURING_OPENFOAM_BUILD"
		if [ -f "$sentinel" ];then
		    echo "patch for $PACKAGE was already applied"
		else
		    echo "apply patch $patchFile for $PACKAGE"
		    touch "$sentinel"
		    patch -b -l -p1 < $patchFile 2>&1 | tee $sentinel
		fi
	    )
	fi
    done

    foamConfigurePathsOptions="\
            --scotchVersion $SCOTCH_PACKAGE \
	    --paraviewVersion ${PARAVIEW_PACKAGE#ParaView-}"

    case "$FOAM_VERSION" in
	v17*|v1806*|v1812*|v1906*|v1912*|v2006*)
	    foamConfigurePathsOptions="\
$foamConfigurePathsOptions \
-boost $BOOST_PACKAGE \
-cgal $CGAL_PACKAGE \
-cmake $CMAKE_PACKAGE \
-fftw $FFTW_PACKAGE \
-openmpi $OPENMPI_PACKAGE \
$GMP_PACKAGE \
$MPFR_PACKAGE \
$MPC_PACKAGE"
	    ;;
    esac

    case "$FOAM_VERSION" in
	v1712*|v1806*|v1812*|v1906*|v2006*)
	    foamConfigurePathsOptions="\
$foamConfigurePathsOptions \
-kahip $KAHIP_PACKAGE"
	    ;;
    esac

    (cd $WM_PROJECT_DIR
	bin/tools/foamConfigurePaths $foamConfigurePathsOptions
    )
}

copy_or_link_ThirdParty_package()
{
    [ "$linkOpt" != true -a "$copyOpt" != true ] && return 1

    local PACKAGE=$1
    local ARCH=$2
    local PATH_TO_THIRDPARTY_PACKAGE=""

    for LINK_VERSION in \
v1912 \
7 \
v1906 \
v1812 \
6 \
v1806 \
v1712 \
v1706 \
5.0 \
v1612+ \
v1606+ \
4.1 \
4.0 \
v3.0+ \
3.0.1 \
3.0.0 \
2.4.0 \
2.3.1 \
2.3.0
    do
	local LINK_PATH=$WM_PROJECT_INST_DIR/ThirdParty-$LINK_VERSION/platforms/$ARCH/$PACKAGE
	if [ -d $WM_PROJECT_INST_DIR/ThirdParty-$LINK_VERSION/platforms/$ARCH/$PACKAGE ];then
	    PATH_TO_THIRDPARTY_PACKAGE=$LINK_PATH
	    break
	fi
    done

    [ -z "$PATH_TO_THIRDPARTY_PACKAGE" ] && return 1

    [ -d $WM_THIRD_PARTY_DIR/platforms/$ARCH ] || mkdir -p $WM_THIRD_PARTY_DIR/platforms/$ARCH
    rm -rf $WM_THIRD_PARTY_DIR/platforms/$ARCH/$PACKAGE
    if [ "$linkOpt" = true ];then
	command="ln -s $PATH_TO_THIRDPARTY_PACKAGE $WM_THIRD_PARTY_DIR/platforms/$ARCH/$PACKAGE"
    elif [ "$copyOpt" = true ];then 
	command="cp -a $PATH_TO_THIRDPARTY_PACKAGE $WM_THIRD_PARTY_DIR/platforms/$ARCH/$PACKAGE"
    fi
    echo $command
    $command

    return 0
}

download_and_extract_package()
{
    local PACKAGE="$1"
    local URL="$2"

    local PACKAGE_FILE="${URL##*/}"
    PACKAGE_FILE="${PACKAGE_FILE%\?*}"

    [ "${PACKAGE%%-*}" = "qt" ] && PACKAGE="qt-everywhere-opensource-src-${PACKAGE#*-}"

    if [ ! -d $WM_THIRD_PARTY_DIR/$PACKAGE ];then
	local source_file=$PACKAGE_FILE
	downloadFile $source_file "$URL"
	tar xpf download/$source_file -C $WM_THIRD_PARTY_DIR
    fi
}

copy_or_link_another_compiler()
{
    [ "$linkOpt" != true -a "$copyOpt" != true ] && return 1

    local COMPILER=$1
    local PACKAGE=$2

    [ $COMPILER_NAME != "Icc" ] && return 1

    GCC_COMPATIBILITY_VERSION=`icc -v  2>&1 |cut -d ' ' -f 6 | tr '.' '_'`

    dir=$WM_THIRD_PARTY_DIR/platforms/${WM_ARCH}Gcc${GCC_COMPATIBILITY_VERSION}

    if expr $COMPILER : "KNL$" > /dev/null;then
	dir=${dir}KNL
    fi

    if [ -d $dir/$PACKAGE ];then
	[ -d $WM_THIRD_PARTY_DIR/platforms/${WM_ARCH}${COMPILER} ] || \
	    mkdir -p $WM_THIRD_PARTY_DIR/platforms/${WM_ARCH}${COMPILER}
	ln -s $dir/$PACKAGE $WM_THIRD_PARTY_DIR/platforms/${WM_ARCH}${COMPILER}/$PACKAGE

	echo "Link to $WM_THIRD_PARTY_DIR/platforms/${WM_ARCH}${COMPILER}/$PACKAGE"

	return 0
    fi

    return 1
}

download_CGAL()
{
    [ "$CGAL_PACKAGE" = "CGAL-none" ] && return 0

    local PACKAGE=$CGAL_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH  ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(cgal_url $PACKAGE)"
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

download_Boost()
{
    [ "$BOOST_PACKAGE" = "boost-none" ] && return 0

    local PACKAGE=$BOOST_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH  ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(boost_url $PACKAGE)"
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

build_Gcc()
{
    [ -n "$RAPIDCFD_VERSION" ] && return 0

    source $WM_PROJECT_DIR/etc/bashrc \
	$foam_settings \
	foamCompiler=system WM_COMPILER_TYPE=system WM_COMPILER=Gcc WM_MPLIB=dummy

    [ -d  $WM_THIRD_PARTY_DIR/platforms ] \
	|| mkdir  $WM_THIRD_PARTY_DIR/platforms

    local GMP_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$GMP_PACKAGE
    if [ ! -d $GMP_ARCH_PATH  ];then
	local GMP_PACKAGE=${GMP_ARCH_PATH##*/}
	local URL="$(gmp_url $GMP_PACKAGE)"
	copy_or_link_ThirdParty_package "$GMP_PACKAGE" "$WM_ARCH" || \
	    download_and_extract_package "$GMP_PACKAGE" "$URL"
    else
	echo "$GMP_PACKAGE package is already installed in $GMP_ARCH_PATH"
    fi

    if [ -d $GMP_ARCH_PATH/lib -a ! -d $GMP_ARCH_PATH/lib64 ];then
	(cd $GMP_ARCH_PATH;rm -f lib64;ln -s lib lib64)
    fi
    if [ -d $GMP_ARCH_PATH/lib64 -a ! -d $GMP_ARCH_PATH/lib ];then
	(cd $GMP_ARCH_PATH;rm -f lib;ln -s lib64 lib)
    fi

    local MPFR_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$MPFR_PACKAGE
    if [ ! -d $MPFR_ARCH_PATH  ];then
	local MPFR_PACKAGE=${MPFR_ARCH_PATH##*/}
	local URL="$(mpfr_url $MPFR_PACKAGE)"
	copy_or_link_ThirdParty_package "$MPFR_PACKAGE" "$WM_ARCH" || \
	    download_and_extract_package "$MPFR_PACKAGE" "$URL"
    else
	echo "$MPFR_PACKAGE package is already installed in $MPFR_ARCH_PATH"
    fi

    if [ -d $MPFR_ARCH_PATH/lib -a ! -d $MPFR_ARCH_PATH/lib64 ];then
	(cd $MPFR_ARCH_PATH;rm -f lib64;ln -s lib lib64)
    fi
    if [ -d $MPFR_ARCH_PATH/lib64 -a ! -d $MPFR_ARCH_PATH/lib ];then
	(cd $MPFR_ARCH_PATH;rm -f lib;ln -s lib64 lib)
    fi

    local MPC_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$MPC_PACKAGE
    if [ ! -d $MPC_ARCH_PATH ];then
	local MPC_PACKAGE=${MPC_ARCH_PATH##*/}
	local URL="$(mpc_url $MPC_PACKAGE)"
	copy_or_link_ThirdParty_package "$MPC_PACKAGE" "$WM_ARCH" || \
	    download_and_extract_package "$MPC_PACKAGE" "$URL"
    else
	echo "$MPC_PACKAGE package is already installed in $MPC_ARCH_PATH"
    fi

    if [ -d $MPC_ARCH_PATH/lib -a ! -d $MPC_ARCH_PATH/lib64 ];then
	(cd $MPC_ARCH_PATH;rm -f lib64;ln -s lib lib64)
    fi
    if [ -d $MPC_ARCH_PATH/lib64 -a ! -d $MPC_ARCH_PATH/lib ];then
	(cd $MPC_ARCH_PATH;rm -f lib;ln -s lib64 lib)
    fi

    local GCC_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$GCC_PACKAGE
    if [ ! -d $GCC_ARCH_PATH -o ! -d $GMP_ARCH_PATH  -o ! -d $MPFR_ARCH_PATH -o ! -d $MPC_ARCH_PATH ];then
	if [ $COMPILER_NAME = "Gcc" -a  $COMPILER_TYPE != "system" ];then
	    local URL="$(gcc_url $GCC_PACKAGE)"
	    copy_or_link_ThirdParty_package "$GCC_PACKAGE" "$WM_ARCH" || \
		download_and_extract_package "$GCC_PACKAGE" "$URL"
        fi

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	    if [ $COMPILER_NAME != "Gcc" -o $COMPILER_TYPE = "system" ];then
	    # make dummy directory
	    [ ! -d $GCC_ARCH_PATH ] && mkdir -p $GCC_ARCH_PATH
	fi
	(cd $WM_THIRD_PARTY_DIR
	    source $WM_PROJECT_DIR/etc/bashrc $foam_settings \
		$foam_settings \
		foamCompiler=system WM_COMPILER_TYPE=system WM_COMPILER=Gcc

	    # make dummy directory
	    [ ! -d ${GCC_ARCH_PATH##*/} ] && mkdir ${GCC_ARCH_PATH##*/}

	    [ -z "$WM_ARCH_OPTION" ] && export WM_ARCH_OPTION=$ARCH_OPTION

	    chmod +x makeGcc
	    bash -e ./makeGcc $GMP_PACKAGE $MPFR_PACKAGE $MPC_PACKAGE $GCC_PACKAGE

	    [ -z "$(ls -A ${GCC_ARCH_PATH##*/})" ] && rmdir ${GCC_ARCH_PATH##*/}
	)
	if [ $COMPILER_NAME != "Gcc" -o $COMPILER_TYPE = "system" ];then
	    [ -z "$(ls -A $GCC_ARCH_PATH/)" ] && rmdir $GCC_ARCH_PATH
	fi
    else
	echo "$GCC_PACKAGE package is already installed in $GCC_ARCH_PATH"
    fi
}

add_wmake_rules()
{
    [ -n "$DOWNLOAD_ONLY" ] && return 0

    local RULES=$WM_PROJECT_DIR/wmake/rules

    local DST_COMPILER=$RULES/$WM_ARCH$WM_COMPILER
    if [ ! -d $DST_COMPILER ];then
	local SRC_COMPILER=$RULES/$WM_ARCH$COMPILER_WM_NAME
	[ -d $SRC_COMPILER ] || error "wmake rule $SRC_COMPILER does not exist"
	if [ "$removexHostOpt" = true ]
	then
	    for file in $SRC_COMPILER/c*
	    do
		sed -i s/-xHost// $file
	    done
	fi
	cp -a $SRC_COMPILER $DST_COMPILER
    fi

    local DST_MPLIB_GENERAL=$RULES/General/mplib$WM_MPLIB
    local MPLIB_ORIG=`echo $WM_MPLIB | tr -d [:digit:][a-z]_`
    if [ ! -f $DST_MPLIB_GENERAL ];then
	local SRC_MPLIB_GENERAL=$RULES/General/mplib$MPLIB_ORIG
	if [ -f $SRC_MPLIB_GENERAL ];then
	    cp -a $SRC_MPLIB_GENERAL $DST_MPLIB_GENERAL
	fi
    fi

    local SRC_MPLIB_COMPILER=$RULES/$WM_ARCH$WM_COMPILER/mplib$MPLIB_ORIG
    if [ -f $SRC_MPLIB_COMPILER ];then
	local DST_MPLIB_COMPILER=$RULES/$WM_ARCH$WM_COMPILER/mplib$WM_MPLIB
	if [ ! -f $DST_MPLIB_COMPILER ];then
	    cp -a $SRC_MPLIB_COMPILER $DST_MPLIB_COMPILER
	fi
	local COMPILER_TYPE_TMP=`echo $WM_COMPILER | tr -d [:digit:]_`
	local DST_MPLIB_COMPILER=$RULES/$WM_ARCH$COMPILER_TYPE_TMP/mplib$WM_MPLIB
	if [ ! -f $DST_MPLIB_COMPILER ];then
	    cp -a $SRC_MPLIB_COMPILER $DST_MPLIB_COMPILER
	fi
    fi
}

download_OpenMPI()
{
    if [ $MPLIB != "OPENMPI" ];then
	echo "Skip download_openmpi since MPLIB is not OPENMPI"
	return 0
    fi

    source $WM_PROJECT_DIR/etc/bashrc $foam_settings

    if [ ! -d $MPI_ARCH_PATH ];then
	local PACKAGE=${MPI_ARCH_PATH##*/}

	# Extract from OpenMPI tar ball if version is 2.4.0 since
        # OpenMPI source in ThirdParty-2.4.0 is incompelete.
	# See https://bugs.openfoam.orig/view.php?id=1770
	if [ $FOAM_VERSION = "2.4.0" ];then
	    if [ ! -d $WM_THIRD_PARTY_DIR/$PACKAGE.orig \
		-a -d $WM_THIRD_PARTY_DIR/$PACKAGE ];then
		mv $WM_THIRD_PARTY_DIR/$PACKAGE \
		    $WM_THIRD_PARTY_DIR/$PACKAGE.orig
	    fi
	fi

	copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" "$(openmpi_url $PACKAGE)"

	rm -f $WM_THIRD_PARTY_DIR/$PACKAGE/Makefile
    else
	echo "OpenMPI package is already installed in $MPI_ARCH_PATH"
    fi
}

download_FFTW()
{
    [ $FFTW_PACKAGE = "fftw-none" ] && return 0
    [ $FFTW_PACKAGE = "fftw-system" ] && return 0

    if [ -n "$FFTW_ARCH_PATH" ];then
	if [ ! -d $FFTW_ARCH_PATH ];then
	    local PACKAGE=${FFTW_ARCH_PATH##*/}
	    local URL="$(fftw_url $PACKAGE)"
	    download_and_extract_package "$PACKAGE" "$URL"
	else
	    echo "$PACKAGE is already installed in $FFTW_ARCH_PATH"
	fi
    else
	echo "Skip downloading FFTW since FFTW_ARCH_PATH is not set"
    fi
}

build_CMake()
{
    [ "$CMAKE_PACKAGE" = "cmake-none" ] && return 0

    local PACKAGE=$CMAKE_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE

    if [ ! -d $ARCH_PATH  ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(cmake_url $PACKAGE)"

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	if [ ! -d $ARCH_PATH  ];then
	    (cd $WM_THIRD_PARTY_DIR
		chmod +x makeCmake
		./makeCmake $PACKAGE
	    )
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

build_ParaView_binutils()
{
    [ "$BINUTILS_PACKAGE" = "binutils-none" ] && return 0
    [ "$BINUTILS_PACKAGE" = "binutils-system" ] && return 0

    local PACKAGE=$BINUTILS_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH  ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(binutils_url $PACKAGE)"

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	if [ ! -d $ARCH_PATH  ];then
	    (cd $WM_THIRD_PARTY_DIR/$PACKAGE
		./configure --prefix=$ARCH_PATH
		make -j $WM_NCOMPPROCS && make install
	    )
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi

    export PATH=$ARCH_PATH/bin:$PATH
    export LD_LIBRARY_PATH=$ARCH_PATH/lib:$LD_LIBRARY_PATH
}

build_ParaView_zlib()
{
    [ "$ZLIB_PACKAGE" = "zlib-none" ] && return 0
    [ "$ZLIB_PACKAGE" = "zlib-system" ] && return 0

    local PACKAGE=$ZLIB_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH  ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(zlib_url $PACKAGE)"

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	if [ ! -d $ARCH_PATH  ];then
	    (cd $WM_THIRD_PARTY_DIR/$ZLIB_PACKAGE
		./configure --prefix=$ARCH_PATH
		make -j $WM_NCOMPPROCS && make install
	    )
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi

    export ZLIB_CFLAGS=" "
    export ZLIB_LIBS="-L$ARCH_PATH/lib -lz"
}

build_ParaView_Qt()
{
    local PACKAGE=$QT_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(qt_url $PACKAGE)"

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	if [ ! -d $ARCH_PATH ];then
	    (cd $WM_THIRD_PARTY_DIR
		source $WM_PROJECT_DIR/etc/bashrc $foam_settings
		chmod +x makeQt
		case "$FOAM_VERSION" in
		    v1[7-9][0-9][0-9]* | v2[0-9][0-9][0-9]* )
			makeQt_options=${PACKAGE}
			;;
		    *)
			makeQt_options=""
			;;
		esac
		  
		./makeQt $makeQt_options
	    )
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

build_ParaView_Python()
{
    [ "$PYTHON_PACKAGE" = "Python-system" ] && return 0

    PYTHON_VERSION=${PYTHON_PACKAGE#Python-}

    local PYTHON_MAJOR_VERSION=${PYTHON_VERSION%.*}
    local PACKAGE=$PYTHON_PACKAGE

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    if [ ! -d $ARCH_PATH ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE || \
	    copy_or_link_ThirdParty_package "$PACKAGE" "$WM_ARCH$WM_COMPILER" || \
	    download_and_extract_package "$PACKAGE" \
	    "$(python_url $PACKAGE)"

	[ -n "$DOWNLOAD_ONLY" ] && return 0

	if [ ! -d $ARCH_PATH ];then
	    (
		cd $WM_THIRD_PARTY_DIR/$PACKAGE
		./configure --prefix=$ARCH_PATH --enable-shared --enable-unicode=ucs4
		make -j $WM_NCOMPPROCS && make altinstall
		(cd $WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE/bin
		    ln -s python$PYTHON_MAJOR_VERSION python
		)
	    )
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

build_ParaView()
{
    local PACKAGE=$PARAVIEW_PACKAGE
    local QMAKE_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$QT_PACKAGE/bin/qmake
    local PYTHON_VERSION=${PYTHON_PACKAGE#Python-}
    local PYTHON_MAJOR_VERSION=${PYTHON_VERSION%.*}

    local ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PACKAGE
    local PYTHON_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$PYTHON_PACKAGE
    if [ ! -d $ARCH_PATH ];then
	copy_or_link_another_compiler $WM_COMPILER $PACKAGE
	if [ $? -eq 1 ];then
	    copy_or_link_ThirdParty_package $PACKAGE $WM_ARCH$WM_COMPILER

	    [ -n "$DOWNLOAD_ONLY" ] && return 0

	    if [ ! -d $ARCH_PATH ];then
		(cd $WM_THIRD_PARTY_DIR
		    source $WM_PROJECT_DIR/etc/bashrc $foam_settings
			
		    if [ $PYTHON_VERSION != "system" ];then
			export PYTHON_INCLUDE=$PYTHON_ARCH_PATH/include/python$PYTHON_MAJOR_VERSION
			export PATH=$PYTHON_ARCH_PATH/bin:$PATH
			local PYTHON_OPTION="-python-lib $PYTHON_ARCH_PATH/lib/libpython$PYTHON_MAJOR_VERSION.so"
    		    fi

		    makeParaViewOptions="\
-cmake $WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$CMAKE_PACKAGE \
-qmake $WM_THIRD_PARTY_DIR/platforms/$WM_ARCH$WM_COMPILER/$QT_PACKAGE/bin/qmake \
-python $PYTHON_OPTION \
-$QT_PACKAGE \
"

		    case "$FOAM_VERSION" in
			v1906*)
			    ;;
			*)
			    makeParaViewOptions="$makeParaViewOptions -mpi" 
			    ;;
		    esac


		    local makeParaView=makeParaView
		    if [ -x makeParaView4 ];then
			makeParaView=makeParaView4
		    fi
		    chmod +x $makeParaView
		    for buildStageOption in -config -make -install
		    do
			command="./$makeParaView $makeParaViewOptions $buildStageOption"
			echo $command
			$command
		    done
		)
	    fi
	fi
    else
	echo "$PACKAGE is already installed in $ARCH_PATH"
    fi
}

build_ThirdParty()
{
    [ -n "$DOWNLOAD_ONLY" ] && return 0

    (cd $WM_THIRD_PARTY_DIR
	source $WM_PROJECT_DIR/etc/bashrc $foam_settings
	chmod +x make[a-zA-Z0-9]*
	[ -n "$PLUS_VERSION" ] && options="-k"
	./Allwmake $options
    )
}

build_OpenFOAM()
{
    [ -n "$DOWNLOAD_ONLY" ] && return 0

    (cd $WM_PROJECT_DIR
	source $WM_PROJECT_DIR/etc/bashrc $foam_settings
	if [ $WM_COMPILER_TYPE = "system" ];then
	    export GMP_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$GMP_PACKAGE
	    export MPFR_ARCH_PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$MPFR_PACKAGE
	fi

	if [ "$FLEX_PACKAGE" != "flex-system" ];then
	    export PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$FLEX_PACKAGE/bin:$PATH
	    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$FLEX_PACKAGE/lib
	fi
	[ -n "$PLUS_VERSION" ] && options="-k"
	if [ "$autoWmakeOpt" = true ];then
	    for APPLICATION in ${APPLICATIONS//,/ }
	    do
		auto_wmake -d -j$WM_NCOMPPROCS $APPLICATION 
	    done
	else
	    ./Allwmake $options
	fi
    )
}

#
# main
#

#- Source settings
if [ -f $INSTALLOPENFOAM_DIR/etc/system.local ]
then
    source $INSTALLOPENFOAM_DIR/etc/system.local
else
    source $INSTALLOPENFOAM_DIR/etc/system
fi

[ -f $INSTALLOPENFOAM_DIR/system/$SYSTEM/bashrc ] \
|| error "$INSTALLOPENFOAM_DIR/system/$SYSTEM/bashrc does not exist"
source $INSTALLOPENFOAM_DIR/system/$SYSTEM/bashrc

# Parse options
unset sourceOpt linkOpt copyOpt removexHostOpt autoWmakeOpt functionObjectsOpt
export WM_CONTINUE_ON_ERROR=true
while [ "$#" -gt 0 ]
do
   case "$1" in
   -h | --help)
      usage
      ;;
   -a)
      [ "$#" -ge 2 ] || usage "'$1' option requires an argument"
      autoWmakeOpt=true
      APPLICATIONS=$2
      shift 2
      ;;
   -c | --copy)
      copyOpt=true
      shift
      ;;
   -e)
      unset WM_CONTINUE_ON_ERROR
      shift
      ;;
   -l | --link)
      linkOpt=true
      shift
      ;;
   -r | --remove-xHost)
      removexHostOpt=true
      shift
      ;;
   -s | --source)
      sourceOpt=true
      shift
      ;;
   -*)
      usage "invalid option '$1'"
      ;;
   *)
      break
      ;;
   esac
done

# link option and copy option are exclusive.
if [ "$linkOpt" = true -a "$copyOpt" = true ];then
    usage
fi

if [ "$sourceOpt" = true ];then
    if [ $# -ne 1 ];then
	echo "One VERSION must be given."
	usage
    fi
else
    if [ $# -eq 0 ];then
	echo "Build all of OpenFOAM_BUILD_OPTION_LIST in $INSTALLOPENFOAM_DIR/system/$SYSTEM/bashrc."
    fi
fi

for OpenFOAM_BUILD_OPTION in ${OpenFOAM_BUILD_OPTION_LIST[@]}
do
    if [ $# -gt 0 ];then
	match=false
	for opt in "$@"
	do
	    if expr "$OpenFOAM_BUILD_OPTION" : "$opt" > /dev/null;then
		match=true
		break
	    fi
	done
	if [ "$match" = false ];then
	    if [ "$sourceOpt" != true ];then
		echo "$OpenFOAM_BUILD_OPTION is not match. Skip."
	    fi
	    continue
	fi
    fi
    OpenFOAM_VERSION=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*OpenFOAM_VERSION=\([^,]*\).*$"/"\1"/`
    COMPILER_TYPE=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*COMPILER_TYPE=\([^,]*\).*"/"\1"/`
    COMPILER=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*COMPILER=\([^,]*\).*"/"\1"/`
    ARCH_OPTION=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*ARCH_OPTION=\([^,]*\).*"/"\1"/`
    PRECISION_OPTION=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*PRECISION_OPTION=\([^,]*\).*"/"\1"/`
    LABEL_SIZE=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*LABEL_SIZE=\([^,]*\).*"/"\1"/`
    COMPILE_OPTION=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*COMPILE_OPTION=\([^,]*\).*"/"\1"/`
    MPLIB=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*MPLIB=\([^,]*\).*"/"\1"/`
    BUILD_PARAVIEW=`echo $OpenFOAM_BUILD_OPTION | sed s/".*[,^]*BUILD_PARAVIEW=\([^,]*\).*"/"\1"/`
    [ "$BUILD_PARAVIEW" != "1" ] && BUILD_PARAVIEW=0

    unset PLUS_VERSION
    unset RAPIDCFD_VERSION
    case "$OpenFOAM_VERSION" in
	OpenFOAM-v[0-9]*)
	    FOAM_VERSION=${OpenFOAM_VERSION#OpenFOAM-}
	    PLUS_VERSION=1
	    ;;
	OpenFOAM-[0-9]*)
	    FOAM_VERSION=${OpenFOAM_VERSION#OpenFOAM-}
	    ;;
	RapidCFD-*)
	    FOAM_VERSION=${OpenFOAM_VERSION#RapidCFD-}
	    RAPIDCFD_VERSION=1
	    ;;
	*)
	    echo "Unknown OpenFOAM version: $OpenFOAM_VERSION"
	    exit 1
	    ;;
    esac

#- Source function of download URL of software package
    source $INSTALLOPENFOAM_DIR/etc/url
    [ -f $INSTALLOPENFOAM_DIR/system/$SYSTEM/url ] \
&& source $INSTALLOPENFOAM_DIR/system/$SYSTEM/url

#- Source function of package software version
    source $INSTALLOPENFOAM_DIR/etc/version
    [ -f $INSTALLOPENFOAM_DIR/system/$SYSTEM/version ] \
&& source $INSTALLOPENFOAM_DIR/system/$SYSTEM/version

#- Compiler type and compiler
    COMPILER_WM_NAME=`echo $COMPILER | tr -d [:digit:]_`
    COMPILER_NAME=${COMPILER_WM_NAME%KNL}
    if [ $COMPILER_NAME = "Gcc" ];then
	GCC_VERSION=${COMPILER%KNL}
	GCC_VERSION=`echo ${GCC_VERSION#Gcc} | tr '_' '.'`
	GCC_PACKAGE=gcc-$GCC_VERSION
    else
    # dummy gcc for other compiler
	GCC_PACKAGE=gcc-4.0.0
    fi

#
#- Start building thirdparty software and OpenFOAM
#

# Make OpenFOAM directory
    [ -d $FOAM_INSTALL_DIRECTORY ] || \
	mkdir $FOAM_INSTALL_DIRECTORY  || \
	error "Could not make $FOAM_INSTALL_DIRECTORY" 
    
#- Allwmake settings
	foam_settings="\
foamCompiler=$COMPILER_TYPE \
WM_COMPILER_TYPE=$COMPILER_TYPE \
WM_COMPILER=$COMPILER \
WM_COMPILE_OPTION=$COMPILE_OPTION \
WM_PRECISION_OPTION=$PRECISION_OPTION \
WM_LABEL_SIZE=$LABEL_SIZE \
WM_ARCH_OPTION=$ARCH_OPTION \
WM_MPLIB=$MPLIB\
"
	if [ "$sourceOpt" != true ];then
	    echo
	    echo "================================================================================"
	    echo "Build $OpenFOAM_VERSION under $foam_settings, WM_NCOMPPROCS=$WM_NCOMPPROCS"
	fi

#- Source compiler and MPI library settings
	source $INSTALLOPENFOAM_DIR/system/$SYSTEM/settings
	
	if [ "$sourceOpt" = true ];then
	    echo source $FOAM_INSTALL_DIRECTORY/$OpenFOAM_VERSION/etc/bashrc $foam_settings
	    source $FOAM_INSTALL_DIRECTORY/$OpenFOAM_VERSION/etc/bashrc $foam_settings
	    if [ "$FLEX_PACKAGE" != "flex-system" ];then
		export PATH=$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$FLEX_PACKAGE/bin:$PATH
		export FLEX_ARCH_INC=-I${ARCH_PATH}/include
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WM_THIRD_PARTY_DIR/platforms/$WM_ARCH/$FLEX_PACKAGE/lib
	    fi
	    return
	fi

#- Directory
	WM_PROJECT_DIR=$FOAM_INSTALL_DIRECTORY/$OpenFOAM_VERSION
	WM_THIRD_PARTY_DIR=$FOAM_INSTALL_DIRECTORY/ThirdParty-$FOAM_VERSION

#- Run function of each build step
	for function in \
	    download_and_extract_source \
	    patch_source \
	    build_Gcc \
	    add_wmake_rules \
	    build_CMake \
	    download_CGAL \
	    download_Boost \
	    download_OpenMPI \
	    download_FFTW \
	    build_ThirdParty \
	    build_ParaView_binutils \
	    build_ParaView_zlib \
	    build_ParaView_Qt \
	    build_ParaView_Python \
	    build_ParaView \
	    build_OpenFOAM
	do
	    echo "--------------------------------------------------------------------------------"

	    if expr $function : "^build_ParaView" > /dev/null;then
		PARAVIEW_VERSION=${PARAVIEW_PACKAGE#ParaView-}
    		if [ $PARAVIEW_VERSION = "4.1.0" ];then
		    echo "Building ParaView-$PARAVIEW_VERSION is not supported."
		    BUILD_PARAVIEW=0
		fi

		if [ "$BUILD_PARAVIEW" != "1" ];then
		    echo "BUILD_PARAVIEW is not set. Skip $function."
		    continue
		fi
	    fi

	    if  [ -n "$RAPIDCFD_VERSION" ]
	    then
		case $function in
		    download_and_extract_source|build_ParaView*)
			continue
			;;
		esac
	    fi

	    echo "Running $function"

	    $function 2>&1 || error "$function failed"

	    if [ ! -n "$DOWNLOAD_ONLY" -a $function = "build_Gcc" ];then
		echo source $WM_PROJECT_DIR/etc/bashrc $foam_settings
		source $WM_PROJECT_DIR/etc/bashrc $foam_settings
		echo "Build using $WM_NCOMPPROCS processor(s)."
	    fi

	done
	echo
	echo "End"
done
