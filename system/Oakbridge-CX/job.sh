#!/bin/sh

MAX_QUEUE=8

options=""
n=1
while [ $n -le $MAX_QUEUE ]
do
    command="pjsub \
--step \
$options \
-g $(id -gn) \
-L rscgrp=debug \
-L node=1 \
-L elapse=00:30:00 \
-S \
./installjob.sh"
  echo $command
  output=`$command`
  echo $output
  jid=`expr "$output" : '.* Job \([0-9]*\)_'`
  options="--sparam jid=$jid"
  n=`expr $n + 1`
done
