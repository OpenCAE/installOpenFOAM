## Oakbridge-CXでのビルド方法

### Oakbridge-CXの特徴

- Oakbridge-CXのログインノードでは並列実行・コンパイルが禁止されており，また負荷がかかるプロセスも動作不可．
- また，計算ノードではファイルのダウンロードが不可．

---

### Oakbridge-CXでのOpenFOAMビルド方針

- ログインノードではOpenFOAMやThirdPartyおよび依存ソフトウェアのインストールファイルのダウンロードとファイルの展開のみを行い，計算ノードを用い並列ビルドを行う．
- また，Oakbridge-CXでは計算ノードから参照できるよう，OpenFOAMをworkファイルシステムにインストールする必要がある．
- しかし，ログインノードからは通常通り```$HOME/OpenFOAM```として参照できるほうが便利．
- 従って，ここでは，OpenFOAMはworkファイルシステム内の計算データ用領域である```/work/グループ名/ユーザー名/OpenFOAM```にインストールし，```$HOME/OpenFOAM```からシンボリックリンクを貼る．

---

### OpenFOAM用ディレクトリの作成

既に```$HOME/OpenFOAM```のディレトリやファイルがある場合には，予め別名にするか別の場所に移動していおいてから以下を実行する．

```bash
mkdir /work/$(id -gn)/$(id -un)/OpenFOAM
```

### Homeファイルシステムからのリンク作成

```bash
ln -s /work/$(id -gn)/$(id -un)/OpenFOAM $HOME/OpenFOAM
```

### レポジトリの取得

```bash
cd /work/$(id -gn)/$(id -un)/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

---

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/Oakbridge-CX/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Oakbridge-CX/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定する．

### インストールファイルのダウンロードと展開

```
./installjob.sh >& log.installjob.sh.0 &
```

---

### 計算ノードでのビルド

```bash
system/Oakbridge-CX/job.sh
```

バッチジョブによリノードが持つコア数でビルドする．
チェーン(ステップ)ジョブを用いて，最大投入ジョブ数(MAX_QUEUE)ジョブを投入する．
