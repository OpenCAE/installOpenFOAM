## Reedbush-Uでのビルド方法

### Reedbush-Uの特徴
- Reedbushのログインノードでは並列実行・コンパイルが禁止されており，また負荷がかかるプロセスも動作不可．
- また，計算ノードではファイルのダウンロードが不可．

----

### Reedbush-UでのOpenFOAMビルド方針

- ログインノードではOpenFOAMやThirdPartyおよび依存ソフトウェアのインストールファイルのダウンロードとファイルの展開のみを行い，バッチジョブを用い36コアの計算ノードで最大36並列の並列ビルドを行う．
- また，Reedbushでは計算ノードから参照できるよう，OpenFOAMをlustreファイルシステムにインストールする必要がある．
- しかし，ログインノードからは通常通り```$HOME/OpenFOAM```として参照できるほうが便利．
- 従って，ここでは，OpenFOAMはlustreファイルシステム内の計算データ用領域である```/lustre/グループ名/ユーザー名/OpenFOAM```にインストールし，```$HOME/OpenFOAM```からシンボリックリンクを貼る．

---

### Reedbush-UでのOpenFOAM用ディレクトリの作成

既に```$HOME/OpenFOAM```のディレトリやファイルがある場合には，予め別名にするか別の場所に移動していおいてから以下を実行．

```bash
mkdir /lustre/$(id -gn)/$(id -un)/OpenFOAM
```

### Reedbush-Uでのhomeファイルシステムからのリンク作成

```bash
ln -s /lustre/$(id -gn)/$(id -un)/OpenFOAM $HOME/OpenFOAM
```

### Reedbush-Uでのレポジトリの取得

```bash
cd /lustre/$(id -gn)/$(id -un)/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

---

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/Reedbush-U/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Reedbush-U/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定する．

### インストールファイルのダウンロードと展開

```
./installjob.sh >& log.installjob.sh.0 &
```

---


### 計算ノードでのビルド

```bash
system/Reedbush-U/job.sh
```

バッチジョブによリ1ノード36並列でビルドする．
チェーンジョブを用いて，最大投入ジョブ数(MAX_QUEUE)ジョブを投入する．
