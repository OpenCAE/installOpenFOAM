#!/bin/sh

MAX_QUEUE=8

options="-W group_list=$(id -gn) -q u-debug -l walltime=0:30:00 -l select=1:mpiprocs=36"
step_options=""
n=1
while [ $n -le $MAX_QUEUE ]
do
    command="qsub $step_options $options ./installjob.sh"
    echo $command
    id=`$command`
    echo $id
    step_options="-W depend=afterany:$id"
    n=`expr $n + 1`
done
