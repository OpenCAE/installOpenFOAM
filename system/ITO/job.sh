#!/bin/bash
export LANG=C
pjsub \
-L "rscgrp=ito-ss" \
-L "rscunit=ito-a" \
-L "vnode=1" \
-L "vnode-core=36" \
-L "elapse=4:00:00" \
--mpi "proc=36" \
-S \
./installjob.sh
