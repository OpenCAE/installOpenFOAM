## ITOでのビルド方法

### ITOの特徴
- 計算ノードでもファイルのダウンロードが可能

## ITOでのOpenFOAMビルド方針
- バッチジョブを用い計算ノードで並列ビルドを行う
- バッチジョブでのビルドではエラーが出る場合や，ジョブが混雑している場合にはログインノードでビルドする

---

### OpenFOAM用ディレクトリの作成

```bash
mkdir ~/OpenFOAM
```

### レポジトリの取得

```bash
cd ~/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/ITO/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/ITO/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定する

### インストールファイルのダウンロードと展開

```
./installjob.sh >& log.install.sh.0 &
```

---

### ITOでのバッチジョブでのビルド

```bash
./system/ITO/job.sh
```

バッチジョブによリノードが持つコア数でビルドする．

