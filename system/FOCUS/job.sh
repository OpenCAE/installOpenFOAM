#!/bin/sh

MAX_QUEUE=5

options="-p f006m -n 1 -J installOpenFOAM -e installOpenFOAM.e%J -o installOpenFOAM.o%J"
step_options=""
n=1
while [ $n -le $MAX_QUEUE ]
do
    command="sbatch $options $step_options ./installjob.sh"
    echo $command
    id=`$command`
    echo $id
    step_options="-d afterany:$id"
    n=`expr $n + 1`
done
