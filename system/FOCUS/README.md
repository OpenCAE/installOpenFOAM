## FOCUSでのビルド方法

### FOCUSの特徴
- Reedbushのフロントエンドノードでは並列実行・コンパイルが禁止されており，また負荷がかかるプロセスも動作不可
- また，FOCUSでは計算ノードに限らず，フロントエンドノードとファイルのダウンロードが不可

### FOCUSでのOpenFOAMビルド方針
- インストールファイルのダウンロードは別のシステムで行い，ダウンロードディレクトリのdownload内のファイルをFOCUSにアップロード
- ログインノードではOpenFOAMやThirdPartyおよび依存ソフトウェアのインストールファイルのファイルの展開のみを行い，バッチジョブを用い計算ノードで並列ビルドを行う

---

### FOCUSでのOpenFOAM用ディレクトリの作成

- 既に```$HOME/OpenFOAM```のディレトリやファイルがある場合には，予め別名にするか別の場所に移動していおいてから以下を実行

```bash
mkdir $HOME/OpenFOAM
```

### FOCUSでのレポジトリの取得

```bash
cd $HOME/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

---

### FOCUSへのインストールファイルのアップロード

FOCUSでインストールファイルを置くディレクトリを作成．

```
mkdir download
```

このディレクトリに，別システムでダウンロードしたインストールファイルをアップードする．


---

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/FOCUS/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/FOCUS/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定．

---

### インストールファイルの展開


```bash
./installjob.sh >& log.installjob.sh.0 &
```

FOCUSのフロントエンドノードで```installjob.sh```を実行すると，ファイルの展開が行われる．

---

### 計算ノードでのビルド

```bash
system/FOCUS/job.sh
```

バッチジョブによリノードが持つコア数でビルドする．
チェーン(ステップ)ジョブを用いて，最大投入ジョブ数(MAX_QUEUE)ジョブを投入する．
