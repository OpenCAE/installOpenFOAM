## Oakforest-PACSでのビルド方法

### Oakforest-PACSの特徴

- Oakforest-PACSのログインノード(CPU: Intel Xeon CPU E5-2690 v4, Broadwell, 2.60GHz)では並列実行・コンパイルが禁止されており，また負荷がかかるプロセスも動作不可．
- また，計算ノードではファイルのダウンロードが不可．
- さらに，コア単体性能が低いXeon Phiの計算ノードでビルドするのは効率が悪い．

---

### Oakforest-PACSでのOpenFOAMビルド方針

- ログインノードではOpenFOAMやThirdPartyおよび依存ソフトウェアのインストールファイルのダウンロードとファイルの展開のみを行い，ログインノードとCPUが同じであるprepostのインタラクティブキューを用い最大28並列の並列ビルドを行う．
- また，Oakforest-PACSでは計算ノードから参照できるよう，OpenFOAMをworkファイルシステムにインストールする必要がある．
- しかし，ログインノードからは通常通り```$HOME/OpenFOAM```として参照できるほうが便利．
- 従って，ここでは，OpenFOAMはworkファイルシステム内の計算データ用領域である```/work/グループ名/ユーザー名/OpenFOAM```にインストールし，```$HOME/OpenFOAM```からシンボリックリンクを貼る．

---

### OpenFOAM用ディレクトリの作成

既に```$HOME/OpenFOAM```のディレトリやファイルがある場合には，予め別名にするか別の場所に移動していおいてから以下を実行する．

```bash
mkdir /work/$(id -gn)/$(id -un)/OpenFOAM
```

### Homeファイルシステムからのリンク作成

```bash
ln -s /work/$(id -gn)/$(id -un)/OpenFOAM $HOME/OpenFOAM
```

### レポジトリの取得

```bash
cd /work/$(id -gn)/$(id -un)/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

---

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/Oakforest-PACS/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/Oakforest-PACS/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定する．

---

### インストールファイルのダウンロードと展開

- Oakforest-PACSのログインノードで```install.sh```を実行すると，ファイルのダウンロードと展開のみが行われる．

```
./installjob.sh >& log.installjob.sh.0 &
```

---

### prepostインタラクティブキューでのビルド

まず，prepostキューのインタラクティブジョブを実行する．もし，```id -gn```の結果と異なるグループ名を指定したい場合には，適宜変更する．なお，prepostキューではトークンは消費されない．

```bash
pjsub --interact -g $(id -gn) -L rscgrp=prepost --mpi proc=28
```

インタラクティブジョブが実行されて，prepost用のノードにログインしたら，通常通り```install.sh```をビルドすると，最大28並列でビルドする．

```bash
./installjob.sh >& log.installjob.sh &
tail -f log.installjob.sh
```

prepostキューの最大経過時間は6時間ですが，制限時間内に途中終了した場合には，prepostキューのインタラクティブジョブを実行から再度やり直す．
