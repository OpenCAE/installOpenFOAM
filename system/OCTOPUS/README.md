## OCTOPUSでのビルド方法

### OCTOPUSの特徴

- OCTOPUSのログインノード(CPU: Intel Xeon Gold 6126 CPU, 2.60GHz)では並列実行・コンパイルが禁止されており，また負荷がかかるプロセスも動作不可．
- また，計算ノードではファイルのダウンロードが不可．
- さらに，計算ノードにはflexがインストールされていないので，一部のライブラリのコンパイルが失敗する(2018年8月1日現在)．

---

### OCTOPUSでのOpenFOAMビルド方針

- 計算ノードのみではビルドができないので，時間はかかるがログインノードのみでコンパイルする．
- OCTOPUSのhomeファイルシステムの容量は100GBと少ないので，workファイルシステムにインストールする．
- しかし，ログインノードからは通常通り```$HOME/OpenFOAM```として参照できるほうが便利．
- 従って，ここでは，OpenFOAMはworkファイルシステム内の計算データ用領域である```/octfs/work/グループ名/ユーザー名/OpenFOAM```にインストールし，```$HOME/OpenFOAM```からシンボリックリンクを貼る．
- OpenFOAM-2.3.0〜3.0.1，v3.0+〜1606+では，Intel Compilerを用いる場合，C/C++コンパイラーの最適化オプションに，搭載されているCPUを調べ、最適なベクトル化オプションを自動的に選択する```-xHost```が付加されているため，Intel Xeon Goldのログインノードにおいてコンパイルすると，Gold用のバイナリが生成され，Xeon Phiノードで動作しない．このため，```-xHost```オプションを削除してコンパイルする．

---

### OpenFOAM用ディレクトリの作成

既に```$HOME/OpenFOAM```のディレトリやファイルがある場合には，予め別名にするか別の場所に移動していおいてから以下を実行する．

```bash
mkdir /octfs/work/$(id -gn)/$(id -un)/OpenFOAM
```

### Homeファイルシステムからのリンク作成

```bash
ln -s /octfs/work/$(id -gn)/$(id -un)/OpenFOAM $HOME/OpenFOAM
```

### レポジトリの取得

```bash
cd /octfs/work/$(id -gn)/$(id -un)/OpenFOAM
git clone https://gitlab.com/OpenCAE/installOpenFOAM.git
cd installOpenFOAM
```

---

### インストールスクリプトの修正

```
vi ./installjob.sh
```

install.shの引数には[system/OCTOPUS/bashrc](https://gitlab.com/OpenCAE/installOpenFOAM/blob/master/system/OCTOPUS/bashrc)におけるOpenFOAM_BUILD_OPTION_LIST配列で定義しているビルドするバージョンを指定する．

OpenFOAM-2.3.0〜3.0.1，v3.0+〜1606+でIntel Compilerを用いる場合，install.shの引数に```-r```または```--remove-xHost```オプションを指定する．

---

### ログインノードでのビルド

```bash
./installjob.sh >& log.installjob.sh &
```
